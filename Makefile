all: build down up

build:
	docker-compose build

up:
	docker-compose up -d

down:
	docker-compose down

register:
	docker-compose run --rm gitlab-runner register
	sudo chown -R ${USER} config/
