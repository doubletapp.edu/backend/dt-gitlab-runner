# DOUBLETAPP GitLab Runner

## Register
```bash
make register
```


1. Enter the GitLab instance URL:\
```
# Go to [ Settings > CI / CD > Runners > Specific runners ] in your project
# In 'Set up a specific runner manually' in 2. get the GitLab instance URL and the registration token
https://gitlab.com/
```

2. Enter the registration token:\
```
smth like Y6XpbN6awmp1LJoiQPSa
```

3. Enter a description for the runner:\
`{project_name}/{env}/{number}`
```
dt-ci-example/test/1
dt-ci-example/prod/1
dt-ci-example/prod/2
```

4. Enter tags for the runner (comma-separated):
```
job:build, job:test, job:deploy.test/1
job:deploy.prod/1
job:deploy.prod/2
```

5. Enter an executor:\
`docker`

6. Enter the default Docker image:\
`alpine:3.13`

7. Update `volumes` variable in the generated config/config.toml to proxy pass docker to the image:
```
volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
```

## Run
```bash
make
```
